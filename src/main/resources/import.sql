-- PERSONA
INSERT INTO public.persona (per_identificacion, per_primer_apellido, per_primer_nombre, per_segundo_apellido, per_segundo_nombre) VALUES ('1075XXXXX', 'apellido 1', 'nombre 1', 'apelido 2', 'nombre 2');

-- TIPO ACTIVO
INSERT INTO public.tipo_activo (tia_nombre) VALUES ('Bienes muebles'), ('Maquinaria'), ('Material de oficina');

-- ESTADO ACTIVO
INSERT INTO public.estado_activo (esa_nombre) VALUES ('Activo'), ('Dado de baja'), ('En reparación'), ('Disponible'), ('Asignado');

-- CIUDAD
INSERT INTO public.ciudad (ciu_nombre) VALUES ('Bogota D.C'), ('Neiva'), ('Bucaramanga'), ('Cartagena'), ('Tolima');

-- AREA
INSERT INTO public.area (ciu_codigo, are_direccion, are_nombre) VALUES (2, 'direccion 1', 'nombre area 1');