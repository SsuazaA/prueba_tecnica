package co.com.grupoasd.prueba_tenica.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

@Entity
@Table(name = "activo")
public class Activo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "act_codigo")
	private Long codigo;

	@Column(name = "act_nombre")
	private String nombre;

	@Column(name = "act_descripcion")
	private String descripcion;

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "tia_codigo")
	private TipoActivo tipoActivo;

	@Column(name = "act_serial")
	private String serial;

	@Column(name = "act_numero_inventario")
	private String numeroInventario;

	@Column(name = "act_peso")
	private int peso;

	@Column(name = "act_alto")
	private int alto;

	@Column(name = "act_ancho")
	private int ancho;

	@Column(name = "act_largo")
	private int largo;

	@Column(name = "act_valor_compra")
	private String valorCompra;

	@Column(name = "act_fecha_compra", columnDefinition = "timestamp")
	private Timestamp fechaCompra;

	@Column(name = "act_fecha_baja", columnDefinition = "timestamp")
	private Timestamp fechaBaja;

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "esa_codigo")
	private EstadoActivo estado;

	@Column(name = "act_color")
	private String color;

	@ManyToOne(fetch = FetchType.EAGER, optional = true)
	@JoinColumn(name = "asa_codigo")
	@Where(clause = "asa_estado = 1")
	private ActivoAsignado asignado;

	/**
	 * 
	 */
	public Activo() {
		super();
	}

	/**
	 * @param codigo
	 */
	public Activo(Long codigo) {
		this.codigo = codigo;
	}

	/**
	 * @param codigo
	 * @param nombre
	 * @param descripcion
	 * @param tipoActivo
	 * @param serial
	 * @param numeroInventario
	 * @param peso
	 * @param alto
	 * @param ancho
	 * @param largo
	 * @param valorCompra
	 * @param fechaCompra
	 * @param fechaBaja
	 * @param estado
	 * @param color
	 */
	public Activo(Long codigo, String nombre, String descripcion, TipoActivo tipoActivo, String serial,
			String numeroInventario, int peso, int alto, int ancho, int largo, String valorCompra,
			Timestamp fechaCompra, Timestamp fechaBaja, EstadoActivo estado, String color) {
		this.codigo = codigo;
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.tipoActivo = tipoActivo;
		this.serial = serial;
		this.numeroInventario = numeroInventario;
		this.peso = peso;
		this.alto = alto;
		this.ancho = ancho;
		this.largo = largo;
		this.valorCompra = valorCompra;
		this.fechaCompra = fechaCompra;
		this.fechaBaja = fechaBaja;
		this.estado = estado;
		this.color = color;
	}

	/**
	 * @param codigo
	 * @param nombre
	 * @param descripcion
	 * @param tipoActivo
	 * @param serial
	 * @param numeroInventario
	 * @param peso
	 * @param alto
	 * @param ancho
	 * @param largo
	 * @param valorCompra
	 * @param fechaCompra
	 * @param fechaBaja
	 * @param estado
	 * @param color
	 * @param asignado
	 */
	public Activo(Long codigo, String nombre, String descripcion, TipoActivo tipoActivo, String serial,
			String numeroInventario, int peso, int alto, int ancho, int largo, String valorCompra,
			Timestamp fechaCompra, Timestamp fechaBaja, EstadoActivo estado, String color, ActivoAsignado asignado) {
		this.codigo = codigo;
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.tipoActivo = tipoActivo;
		this.serial = serial;
		this.numeroInventario = numeroInventario;
		this.peso = peso;
		this.alto = alto;
		this.ancho = ancho;
		this.largo = largo;
		this.valorCompra = valorCompra;
		this.fechaCompra = fechaCompra;
		this.fechaBaja = fechaBaja;
		this.estado = estado;
		this.color = color;
		this.asignado = asignado;
	}

	/**
	 * @return the codigo
	 */
	public Long getCodigo() {
		return codigo;
	}

	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @return the tipoActivo
	 */
	public TipoActivo getTipoActivo() {
		return tipoActivo;
	}

	/**
	 * @param tipoActivo the tipoActivo to set
	 */
	public void setTipoActivo(TipoActivo tipoActivo) {
		this.tipoActivo = tipoActivo;
	}

	/**
	 * @return the serial
	 */
	public String getSerial() {
		return serial;
	}

	/**
	 * @param serial the serial to set
	 */
	public void setSerial(String serial) {
		this.serial = serial;
	}

	/**
	 * @return the numeroInventario
	 */
	public String getNumeroInventario() {
		return numeroInventario;
	}

	/**
	 * @param numeroInventario the numeroInventario to set
	 */
	public void setNumeroInventario(String numeroInventario) {
		this.numeroInventario = numeroInventario;
	}

	/**
	 * @return the peso
	 */
	public int getPeso() {
		return peso;
	}

	/**
	 * @param peso the peso to set
	 */
	public void setPeso(int peso) {
		this.peso = peso;
	}

	/**
	 * @return the alto
	 */
	public int getAlto() {
		return alto;
	}

	/**
	 * @param alto the alto to set
	 */
	public void setAlto(int alto) {
		this.alto = alto;
	}

	/**
	 * @return the ancho
	 */
	public int getAncho() {
		return ancho;
	}

	/**
	 * @param ancho the ancho to set
	 */
	public void setAncho(int ancho) {
		this.ancho = ancho;
	}

	/**
	 * @return the largo
	 */
	public int getLargo() {
		return largo;
	}

	/**
	 * @param largo the largo to set
	 */
	public void setLargo(int largo) {
		this.largo = largo;
	}

	/**
	 * @return the valorCompra
	 */
	public String getValorCompra() {
		return valorCompra;
	}

	/**
	 * @param valorCompra the valorCompra to set
	 */
	public void setValorCompra(String valorCompra) {
		this.valorCompra = valorCompra;
	}

	/**
	 * @return the fechaCompra
	 */
	public Timestamp getFechaCompra() {
		return fechaCompra;
	}

	/**
	 * @param fechaCompra the fechaCompra to set
	 */
	public void setFechaCompra(Timestamp fechaCompra) {
		this.fechaCompra = fechaCompra;
	}

	/**
	 * @return the fechaBaja
	 */
	public Timestamp getFechaBaja() {
		return fechaBaja;
	}

	/**
	 * @param fechaBaja the fechaBaja to set
	 */
	public void setFechaBaja(Timestamp fechaBaja) {
		this.fechaBaja = fechaBaja;
	}

	/**
	 * @return the estado
	 */
	public EstadoActivo getEstado() {
		return estado;
	}

	/**
	 * @param estado the estado to set
	 */
	public void setEstado(EstadoActivo estado) {
		this.estado = estado;
	}

	/**
	 * @return the color
	 */
	public String getColor() {
		return color;
	}

	/**
	 * @param color the color to set
	 */
	public void setColor(String color) {
		this.color = color;
	}

	/**
	 * @return the asignado
	 */
	public ActivoAsignado getAsignado() {
		return asignado;
	}

	/**
	 * @param asignado the asignado to set
	 */
	public void setAsignado(ActivoAsignado asignado) {
		asignado.setActivo(this);
		this.asignado = asignado;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + alto;
		result = prime * result + ancho;
		result = prime * result + ((asignado == null) ? 0 : asignado.hashCode());
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		result = prime * result + ((color == null) ? 0 : color.hashCode());
		result = prime * result + ((descripcion == null) ? 0 : descripcion.hashCode());
		result = prime * result + ((estado == null) ? 0 : estado.hashCode());
		result = prime * result + ((fechaBaja == null) ? 0 : fechaBaja.hashCode());
		result = prime * result + ((fechaCompra == null) ? 0 : fechaCompra.hashCode());
		result = prime * result + largo;
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		result = prime * result + ((numeroInventario == null) ? 0 : numeroInventario.hashCode());
		result = prime * result + peso;
		result = prime * result + ((serial == null) ? 0 : serial.hashCode());
		result = prime * result + ((tipoActivo == null) ? 0 : tipoActivo.hashCode());
		result = prime * result + ((valorCompra == null) ? 0 : valorCompra.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Activo)) {
			return false;
		}
		Activo other = (Activo) obj;
		if (alto != other.alto) {
			return false;
		}
		if (ancho != other.ancho) {
			return false;
		}
		if (asignado == null) {
			if (other.asignado != null) {
				return false;
			}
		} else if (!asignado.equals(other.asignado)) {
			return false;
		}
		if (codigo == null) {
			if (other.codigo != null) {
				return false;
			}
		} else if (!codigo.equals(other.codigo)) {
			return false;
		}
		if (color == null) {
			if (other.color != null) {
				return false;
			}
		} else if (!color.equals(other.color)) {
			return false;
		}
		if (descripcion == null) {
			if (other.descripcion != null) {
				return false;
			}
		} else if (!descripcion.equals(other.descripcion)) {
			return false;
		}
		if (estado == null) {
			if (other.estado != null) {
				return false;
			}
		} else if (!estado.equals(other.estado)) {
			return false;
		}
		if (fechaBaja == null) {
			if (other.fechaBaja != null) {
				return false;
			}
		} else if (!fechaBaja.equals(other.fechaBaja)) {
			return false;
		}
		if (fechaCompra == null) {
			if (other.fechaCompra != null) {
				return false;
			}
		} else if (!fechaCompra.equals(other.fechaCompra)) {
			return false;
		}
		if (largo != other.largo) {
			return false;
		}
		if (nombre == null) {
			if (other.nombre != null) {
				return false;
			}
		} else if (!nombre.equals(other.nombre)) {
			return false;
		}
		if (numeroInventario == null) {
			if (other.numeroInventario != null) {
				return false;
			}
		} else if (!numeroInventario.equals(other.numeroInventario)) {
			return false;
		}
		if (peso != other.peso) {
			return false;
		}
		if (serial == null) {
			if (other.serial != null) {
				return false;
			}
		} else if (!serial.equals(other.serial)) {
			return false;
		}
		if (tipoActivo == null) {
			if (other.tipoActivo != null) {
				return false;
			}
		} else if (!tipoActivo.equals(other.tipoActivo)) {
			return false;
		}
		if (valorCompra == null) {
			if (other.valorCompra != null) {
				return false;
			}
		} else if (!valorCompra.equals(other.valorCompra)) {
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Activo [codigo=" + codigo + ", nombre=" + nombre + ", descripcion=" + descripcion + ", tipoActivo="
				+ tipoActivo + ", serial=" + serial + ", numeroInventario=" + numeroInventario + ", peso=" + peso
				+ ", alto=" + alto + ", ancho=" + ancho + ", largo=" + largo + ", valorCompra=" + valorCompra
				+ ", fechaCompra=" + fechaCompra + ", fechaBaja=" + fechaBaja + ", estado=" + estado + ", color="
				+ color + ", asignado=" + asignado + "]";
	}

}
