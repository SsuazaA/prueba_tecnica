package co.com.grupoasd.prueba_tenica.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.ColumnDefault;

@Entity
@Table(name = "tipo_activo")
public class TipoActivo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "tia_codigo")
	private Long codigo;

	@Column(name = "tia_nombre")
	@NotNull(message = "el nombre no puede ser vació.")
	private String nombre;

	@Column(name = "tia_estado", nullable = false)
	@Min(value = 0, message = "el estado solo puede ser 0 - inactivo, 1 - activo.")
	@Max(value = 1, message = "el estado solo puede ser 0 - inactivo, 1 - activo.")
	@ColumnDefault(value = "1")
	private int estado;

	/**
	 * 
	 */
	public TipoActivo() {
		super();
	}
	
	/**
	 * @param codigo
	 */
	public TipoActivo(Long codigo) {
		this.codigo = codigo;
	}

	/**
	 * @param codigo
	 * @param nombre
	 * @param estado
	 */
	public TipoActivo(Long codigo, String nombre, int estado) {
		this.codigo = codigo;
		this.nombre = nombre;
		this.estado = estado;
	}

	/**
	 * @return the codigo
	 */
	public Long getCodigo() {
		return codigo;
	}

	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the estado
	 */
	public int getEstado() {
		return estado;
	}

	/**
	 * @param estado the estado to set
	 */
	public void setEstado(int estado) {
		this.estado = estado;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		result = prime * result + estado;
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof TipoActivo))
			return false;
		TipoActivo other = (TipoActivo) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		if (estado != other.estado)
			return false;
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (!nombre.equals(other.nombre))
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TipoActivo [codigo=" + codigo + ", nombre=" + nombre + ", estado=" + estado + "]";
	}

}
