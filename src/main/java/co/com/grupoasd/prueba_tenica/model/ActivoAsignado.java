package co.com.grupoasd.prueba_tenica.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "activo_asignado")
public class ActivoAsignado implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "aca_codigo")
	private Long codigo;

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "act_codigo")
	@JsonBackReference
	private Activo activo;

	@ManyToOne(fetch = FetchType.EAGER, optional = true)
	@JoinColumn(name = "per_codigo")
	private Persona persona;

	@ManyToOne(fetch = FetchType.EAGER, optional = true)
	@JoinColumn(name = "are_codigo")
	private Area area;

	@Column(name = "esa_estado", nullable = false)
	@ColumnDefault(value = "1")
	private int estado;

	/**
	 * 
	 */
	public ActivoAsignado() {
		super();
	}

	/**
	 * @param codigo
	 */
	public ActivoAsignado(Long codigo) {
		this.codigo = codigo;
	}

	/**
	 * @param codigo
	 * @param activo
	 * @param persona
	 * @param area
	 * @param estado
	 */
	public ActivoAsignado(Long codigo, Activo activo, Persona persona, Area area, int estado) {
		this.codigo = codigo;
		this.activo = activo;
		this.persona = persona;
		this.area = area;
		this.estado = estado;
	}

	/**
	 * @return the codigo
	 */
	public Long getCodigo() {
		return codigo;
	}

	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	/**
	 * @return the activo
	 */
	public Activo getActivo() {
		return activo;
	}

	/**
	 * @param activo the activo to set
	 */
	public void setActivo(Activo activo) {
		this.activo = activo;
	}

	/**
	 * @return the persona
	 */
	public Persona getPersona() {
		return persona;
	}

	/**
	 * @param persona the persona to set
	 */
	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	/**
	 * @return the area
	 */
	public Area getArea() {
		return area;
	}

	/**
	 * @param area the area to set
	 */
	public void setArea(Area area) {
		this.area = area;
	}

	/**
	 * @return the estado
	 */
	public int getEstado() {
		return estado;
	}

	/**
	 * @param estado the estado to set
	 */
	public void setEstado(int estado) {
		this.estado = estado;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((activo == null) ? 0 : activo.hashCode());
		result = prime * result + ((area == null) ? 0 : area.hashCode());
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		result = prime * result + estado;
		result = prime * result + ((persona == null) ? 0 : persona.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof ActivoAsignado)) {
			return false;
		}
		ActivoAsignado other = (ActivoAsignado) obj;
		if (activo == null) {
			if (other.activo != null) {
				return false;
			}
		} else if (!activo.equals(other.activo)) {
			return false;
		}
		if (area == null) {
			if (other.area != null) {
				return false;
			}
		} else if (!area.equals(other.area)) {
			return false;
		}
		if (codigo == null) {
			if (other.codigo != null) {
				return false;
			}
		} else if (!codigo.equals(other.codigo)) {
			return false;
		}
		if (estado != other.estado) {
			return false;
		}
		if (persona == null) {
			if (other.persona != null) {
				return false;
			}
		} else if (!persona.equals(other.persona)) {
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ActivoAsignado [codigo=" + codigo + ", activo=" + activo + ", persona=" + persona + ", area=" + area
				+ ", estado=" + estado + "]";
	}

}
