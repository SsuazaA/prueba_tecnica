package co.com.grupoasd.prueba_tenica.service;

import co.com.grupoasd.prueba_tenica.model.Area;
import co.com.grupoasd.prueba_tenica.service.util.CrudServiceInterface;
import co.com.grupoasd.prueba_tenica.service.util.ListServiceInterface;

public interface AreaService extends CrudServiceInterface<Area, Long>, ListServiceInterface<Area, Long> {

}
