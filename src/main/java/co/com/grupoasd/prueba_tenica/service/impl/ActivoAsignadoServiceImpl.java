package co.com.grupoasd.prueba_tenica.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.grupoasd.prueba_tenica.dao.ActivoAsignadoDao;
import co.com.grupoasd.prueba_tenica.model.ActivoAsignado;
import co.com.grupoasd.prueba_tenica.service.ActivoAsignadoService;

@Service
public class ActivoAsignadoServiceImpl implements ActivoAsignadoService {

	@Autowired
	ActivoAsignadoDao activoAsignadoDao;

	@Override
	@Transactional
	public ActivoAsignado agregar(ActivoAsignado t) {
		return activoAsignadoDao.save(t);
	}

	@Override
	@Transactional
	public ActivoAsignado modificar(ActivoAsignado t) {
		return activoAsignadoDao.save(t);
	}
	
	@Override
	@Transactional
	public ActivoAsignado activar(ActivoAsignado t) {
		if (t.getEstado() == 1) {
			return t;
		} else {
			t.setEstado(1);
			return activoAsignadoDao.save(t);
		}
	}

	@Override
	@Transactional
	public ActivoAsignado inactivar(ActivoAsignado t) {
		if (t.getEstado() == 0) {
			return t;
		} else {
			t.setEstado(0);
			return activoAsignadoDao.save(t);
		}
	}

	@Override
	@Transactional(readOnly = true)
	public Optional<ActivoAsignado> buscarPorId(Long id) {
		return activoAsignadoDao.findById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public List<ActivoAsignado> listar() {
		return activoAsignadoDao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Page<ActivoAsignado> listar(Pageable pageable) {
		return activoAsignadoDao.findAll(pageable);
	}

}
