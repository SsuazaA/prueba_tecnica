package co.com.grupoasd.prueba_tenica.service.util;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ListServiceInterface<T, ID> {

	public Optional<T> buscarPorId(ID id);
	
	public List<T> listar();
	
	public Page<T> listar(Pageable pageable);

}
