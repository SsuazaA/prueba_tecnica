package co.com.grupoasd.prueba_tenica.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.grupoasd.prueba_tenica.dao.AreaDao;
import co.com.grupoasd.prueba_tenica.model.Area;
import co.com.grupoasd.prueba_tenica.service.AreaService;

@Service
public class AreaServiceImpl implements AreaService {

	@Autowired
	AreaDao areaDao;

	@Override
	@Transactional
	public Area agregar(Area t) {
		return areaDao.save(t);
	}

	@Override
	@Transactional
	public Area modificar(Area t) {
		return areaDao.save(t);
	}

	@Override
	@Transactional(readOnly = true)
	public Optional<Area> buscarPorId(Long id) {
		return areaDao.findById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Area> listar() {
		return areaDao.findAllByOrderByNombreAsc();
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Area> listar(Pageable pageable) {
		return areaDao.findAll(pageable);
	}

}
