package co.com.grupoasd.prueba_tenica.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.grupoasd.prueba_tenica.dao.PersonaDao;
import co.com.grupoasd.prueba_tenica.model.Persona;
import co.com.grupoasd.prueba_tenica.service.PersonaService;

@Service
public class PersonaServiceImpl implements PersonaService {

	@Autowired
	PersonaDao personaDao;

	@Override
	@Transactional
	public Persona agregar(Persona t) {
		return personaDao.save(t);
	}

	@Override
	@Transactional
	public Persona modificar(Persona t) {
		return personaDao.save(t);
	}

	@Override
	@Transactional(readOnly = true)
	public Optional<Persona> buscarPorId(Long id) {
		return personaDao.findById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Persona> listar() {
		return personaDao.findAllByOrderByCodigoAsc();
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Persona> listar(Pageable pageable) {
		return personaDao.findAll(pageable);
	}

}
