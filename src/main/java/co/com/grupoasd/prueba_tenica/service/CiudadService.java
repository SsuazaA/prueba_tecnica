package co.com.grupoasd.prueba_tenica.service;

import co.com.grupoasd.prueba_tenica.model.Ciudad;
import co.com.grupoasd.prueba_tenica.service.util.ListServiceInterface;

public interface CiudadService extends ListServiceInterface<Ciudad, Long> {

}
