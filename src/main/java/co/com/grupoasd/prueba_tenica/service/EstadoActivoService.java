package co.com.grupoasd.prueba_tenica.service;

import co.com.grupoasd.prueba_tenica.model.EstadoActivo;
import co.com.grupoasd.prueba_tenica.service.util.CrudServiceInterface;
import co.com.grupoasd.prueba_tenica.service.util.EstadoServiceInterface;
import co.com.grupoasd.prueba_tenica.service.util.ListServiceInterface;

public interface EstadoActivoService extends CrudServiceInterface<EstadoActivo, Long>,
		EstadoServiceInterface<EstadoActivo, Long>, ListServiceInterface<EstadoActivo, Long> {

}
