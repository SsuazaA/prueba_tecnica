package co.com.grupoasd.prueba_tenica.service.util;

public interface CrudServiceInterface<T, ID> {

	public T agregar(T t);

	public T modificar(T t);

}
