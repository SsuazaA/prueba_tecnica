package co.com.grupoasd.prueba_tenica.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.grupoasd.prueba_tenica.dao.CiudadDao;
import co.com.grupoasd.prueba_tenica.model.Ciudad;
import co.com.grupoasd.prueba_tenica.service.CiudadService;

@Service
public class CiudadServiceImpl implements CiudadService {

	@Autowired
	CiudadDao ciudadDao;

	@Override
	@Transactional(readOnly = true)
	public Optional<Ciudad> buscarPorId(Long id) {
		return ciudadDao.findById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Ciudad> listar() {
		return ciudadDao.findAllByOrderByNombreAsc();
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Ciudad> listar(Pageable pageable) {
		return ciudadDao.findAll(pageable);
	}

}
