package co.com.grupoasd.prueba_tenica.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.grupoasd.prueba_tenica.dao.ActivoDao;
import co.com.grupoasd.prueba_tenica.model.Activo;
import co.com.grupoasd.prueba_tenica.service.ActivoAsignadoService;
import co.com.grupoasd.prueba_tenica.service.ActivoService;

@Service
public class ActivoServiceImpl implements ActivoService {

	@Autowired
	ActivoDao activoDao;

	@Autowired
	ActivoAsignadoService activoAsignadoService;

	@Override
	@Transactional
	public Activo agregar(Activo t) {
		Activo activo_ = activoDao.save(t);
		activo_.setAsignado(activoAsignadoService.agregar(t.getAsignado()));
		return activo_;
	}

	@Override
	@Transactional
	public Activo modificar(Activo t) {
		Activo activo_ = activoDao.save(t);
		if (activo_.getEstado().getCodigo() == 5) {
			activo_.setAsignado(activoAsignadoService.modificar(t.getAsignado()));
		} else {
			activo_.setAsignado(activoAsignadoService.inactivar(t.getAsignado()));
		}
		return activo_;
	}

	@Override
	@Transactional(readOnly = true)
	public Optional<Activo> buscarPorId(Long id) {
		return activoDao.findById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Activo> listar() {
		return activoDao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Activo> listar(Pageable pageable) {
		return activoDao.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Activo> listarPorEstado(long estado, Pageable pageable) {
		List<Activo> list = activoDao.findAllByEstadoCodigo(estado);
		int start = (int) pageable.getOffset();
		int end = (start + pageable.getPageSize()) > list.size() ? list.size() : (start + pageable.getPageSize());
		return new PageImpl<Activo>(list.subList(start, end), pageable, list.size());
	}

}
