package co.com.grupoasd.prueba_tenica.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.grupoasd.prueba_tenica.dao.EstadoActivoDao;
import co.com.grupoasd.prueba_tenica.model.EstadoActivo;
import co.com.grupoasd.prueba_tenica.service.EstadoActivoService;

@Service
public class EstadoActivoServiceImpl implements EstadoActivoService {

	@Autowired
	EstadoActivoDao estadoActivoDao;

	@Override
	@Transactional
	public EstadoActivo agregar(EstadoActivo t) {
		return estadoActivoDao.save(t);
	}

	@Override
	@Transactional
	public EstadoActivo modificar(EstadoActivo t) {
		return estadoActivoDao.save(t);
	}

	@Override
	@Transactional
	public EstadoActivo activar(EstadoActivo t) {
		if (t.getEstado() == 1) {
			return t;
		} else {
			t.setEstado(1);
			return estadoActivoDao.save(t);
		}
	}

	@Override
	@Transactional
	public EstadoActivo inactivar(EstadoActivo t) {
		if (t.getEstado() == 0) {
			return t;
		} else {
			t.setEstado(0);
			return estadoActivoDao.save(t);
		}
	}

	@Override
	@Transactional(readOnly = true)
	public Optional<EstadoActivo> buscarPorId(Long id) {
		return estadoActivoDao.findById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public List<EstadoActivo> listar() {
		return estadoActivoDao.findAllByEstadoOrderByCodigoAsc(1);
	}

	@Override
	@Transactional(readOnly = true)
	public Page<EstadoActivo> listar(Pageable pageable) {
		return estadoActivoDao.findAll(pageable);
	}

}
