package co.com.grupoasd.prueba_tenica.service.util;

public interface EstadoServiceInterface<T, ID> {

	public T activar(T t);

	public T inactivar(T t);

}
