package co.com.grupoasd.prueba_tenica.service;

import co.com.grupoasd.prueba_tenica.model.ActivoAsignado;
import co.com.grupoasd.prueba_tenica.service.util.CrudServiceInterface;
import co.com.grupoasd.prueba_tenica.service.util.EstadoServiceInterface;
import co.com.grupoasd.prueba_tenica.service.util.ListServiceInterface;

public interface ActivoAsignadoService extends CrudServiceInterface<ActivoAsignado, Long>,
		EstadoServiceInterface<ActivoAsignado, Long>, ListServiceInterface<ActivoAsignado, Long> {

}
