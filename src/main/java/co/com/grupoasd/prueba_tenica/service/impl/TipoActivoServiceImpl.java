package co.com.grupoasd.prueba_tenica.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.grupoasd.prueba_tenica.dao.TipoActivoDao;
import co.com.grupoasd.prueba_tenica.model.TipoActivo;
import co.com.grupoasd.prueba_tenica.service.TipoActivoService;

@Service
public class TipoActivoServiceImpl implements TipoActivoService {

	@Autowired
	TipoActivoDao tipoActivoDao;

	@Override
	@Transactional
	public TipoActivo agregar(TipoActivo t) {
		return tipoActivoDao.save(t);
	}

	@Override
	@Transactional
	public TipoActivo modificar(TipoActivo t) {
		return tipoActivoDao.save(t);
	}

	@Override
	@Transactional
	public TipoActivo activar(TipoActivo t) {
		if (t.getEstado() == 1) {
			return t;
		} else {
			t.setEstado(1);
			return tipoActivoDao.save(t);
		}
	}

	@Override
	@Transactional
	public TipoActivo inactivar(TipoActivo t) {
		if (t.getEstado() == 0) {
			return t;
		} else {
			t.setEstado(0);
			return tipoActivoDao.save(t);
		}
	}

	@Override
	@Transactional(readOnly = true)
	public Optional<TipoActivo> buscarPorId(Long id) {
		return tipoActivoDao.findById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public List<TipoActivo> listar() {
		return tipoActivoDao.findAllByEstadoOrderByNombreAsc(1);
	}

	@Override
	@Transactional(readOnly = true)
	public Page<TipoActivo> listar(Pageable pageable) {
		return tipoActivoDao.findAll(pageable);
	}

}
