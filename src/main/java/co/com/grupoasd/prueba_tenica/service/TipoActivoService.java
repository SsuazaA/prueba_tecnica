package co.com.grupoasd.prueba_tenica.service;

import co.com.grupoasd.prueba_tenica.model.TipoActivo;
import co.com.grupoasd.prueba_tenica.service.util.CrudServiceInterface;
import co.com.grupoasd.prueba_tenica.service.util.EstadoServiceInterface;
import co.com.grupoasd.prueba_tenica.service.util.ListServiceInterface;

public interface TipoActivoService extends CrudServiceInterface<TipoActivo, Long>,
		EstadoServiceInterface<TipoActivo, Long>, ListServiceInterface<TipoActivo, Long> {

}
