package co.com.grupoasd.prueba_tenica.service;

import co.com.grupoasd.prueba_tenica.model.Persona;
import co.com.grupoasd.prueba_tenica.service.util.CrudServiceInterface;
import co.com.grupoasd.prueba_tenica.service.util.ListServiceInterface;

public interface PersonaService extends CrudServiceInterface<Persona, Long>, ListServiceInterface<Persona, Long> {

}
