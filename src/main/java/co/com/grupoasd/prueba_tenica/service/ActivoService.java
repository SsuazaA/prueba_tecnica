package co.com.grupoasd.prueba_tenica.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import co.com.grupoasd.prueba_tenica.model.Activo;
import co.com.grupoasd.prueba_tenica.service.util.CrudServiceInterface;
import co.com.grupoasd.prueba_tenica.service.util.ListServiceInterface;

public interface ActivoService extends CrudServiceInterface<Activo, Long>, ListServiceInterface<Activo, Long> {

	public Page<Activo> listarPorEstado(long estado, Pageable pageable);
	
}
