package co.com.grupoasd.prueba_tenica.web.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import co.com.grupoasd.prueba_tenica.model.Mensaje;
import co.com.grupoasd.prueba_tenica.model.TipoActivo;
import co.com.grupoasd.prueba_tenica.service.TipoActivoService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("api/activo/tipo")
public class TipoActivoController {

	@Autowired
	TipoActivoService tipoActivoService;

	@GetMapping(value = "id/{codigo}")
	@ResponseBody
	public ResponseEntity<Object> buscarPorId(@PathVariable("codigo") long codigo) {
		Optional<TipoActivo> object = tipoActivoService.buscarPorId(codigo);
		return object.isPresent() ? new ResponseEntity<Object>(object, HttpStatus.OK)
				: new ResponseEntity<Object>(new Mensaje(false, "Ningún registro encontrado."), HttpStatus.NOT_FOUND);
	}

	@GetMapping
	@ResponseBody
	public List<TipoActivo> listar() {
		return tipoActivoService.listar();
	}

	@GetMapping(value = "paginacion")
	@ResponseBody
	public Page<TipoActivo> listarPaginado(@PageableDefault(size = 5) @SortDefault.SortDefaults({
			@SortDefault(sort = "codigo", direction = Sort.Direction.ASC) }) Pageable pageable) {
		return tipoActivoService.listar(pageable);
	}

	@PostMapping
	@ResponseBody
	public ResponseEntity<Object> agregar(@Valid @RequestBody TipoActivo tipoActivo, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return new ResponseEntity<Object>(
					new Mensaje(false, "Error " + bindingResult.getAllErrors().get(0).getDefaultMessage()),
					HttpStatus.BAD_REQUEST);
		} else {
			return tipoActivoService.agregar(tipoActivo).getCodigo() != 0
					? new ResponseEntity<Object>(new Mensaje(true, "Tipo activo agregado con exito."), HttpStatus.OK)
					: new ResponseEntity<Object>(new Mensaje(false, "Error al agregar el tipo activo."),
							HttpStatus.BAD_REQUEST);
		}
	}

	@PutMapping
	@ResponseBody
	public ResponseEntity<Object> modificar(@Valid @RequestBody TipoActivo tipoActivo, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return new ResponseEntity<Object>(
					new Mensaje(false, "Error " + bindingResult.getAllErrors().get(0).getDefaultMessage()),
					HttpStatus.BAD_REQUEST);
		} else {
			return tipoActivoService.modificar(tipoActivo).equals(tipoActivo)
					? new ResponseEntity<Object>(new Mensaje(true, "Tipo activo modificado con exito."), HttpStatus.OK)
					: new ResponseEntity<Object>(new Mensaje(false, "Error al modificar el tipo activo."),
							HttpStatus.BAD_REQUEST);
		}

	}

	@PutMapping(value = "{codigo}/activar")
	@ResponseBody
	public ResponseEntity<Object> activar(@PathVariable("codigo") long codigo) {
		Optional<TipoActivo> object = tipoActivoService.buscarPorId(codigo);
		if (object.isPresent()) {
			return tipoActivoService.activar(object.get()).getEstado() == 1
					? new ResponseEntity<Object>(new Mensaje(true, "Tipo activo activado con exito."), HttpStatus.OK)
					: new ResponseEntity<Object>(new Mensaje(false, "Error al activar el tipo activo."),
							HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<Object>(new Mensaje(false, "El tipo activo no existe."), HttpStatus.BAD_REQUEST);
		}

	}

	@PutMapping(value = "{codigo}/inactivar")
	@ResponseBody
	public ResponseEntity<Object> inactivar(@PathVariable("codigo") long codigo) {
		Optional<TipoActivo> object = tipoActivoService.buscarPorId(codigo);
		if (object.isPresent()) {
			return tipoActivoService.inactivar(object.get()).getEstado() == 0
					? new ResponseEntity<Object>(new Mensaje(true, "Tipo activo inactivado con exito."), HttpStatus.OK)
					: new ResponseEntity<Object>(new Mensaje(false, "Error al inactivar el tipo activo."),
							HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<Object>(new Mensaje(false, "El tipo activo no existe."), HttpStatus.BAD_REQUEST);
		}
	}

}
