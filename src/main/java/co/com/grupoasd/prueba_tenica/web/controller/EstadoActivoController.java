package co.com.grupoasd.prueba_tenica.web.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import co.com.grupoasd.prueba_tenica.model.EstadoActivo;
import co.com.grupoasd.prueba_tenica.model.Mensaje;
import co.com.grupoasd.prueba_tenica.service.EstadoActivoService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("api/activo/estado")
public class EstadoActivoController {

	@Autowired
	EstadoActivoService estadoActivoService;

	@GetMapping(value = "id/{codigo}")
	@ResponseBody
	public ResponseEntity<Object> buscarPorId(@PathVariable("codigo") long codigo) {
		Optional<EstadoActivo> object = estadoActivoService.buscarPorId(codigo);
		return object.isPresent() ? new ResponseEntity<Object>(object, HttpStatus.OK)
				: new ResponseEntity<Object>(new Mensaje(false, "Ningún registro encontrado."), HttpStatus.NOT_FOUND);
	}

	@GetMapping
	@ResponseBody
	public List<EstadoActivo> listar() {
		return estadoActivoService.listar();
	}

	@GetMapping(value = "paginacion")
	@ResponseBody
	public Page<EstadoActivo> listarPaginado(@PageableDefault(size = 5) @SortDefault.SortDefaults({
			@SortDefault(sort = "codigo", direction = Sort.Direction.ASC) }) Pageable pageable) {
		return estadoActivoService.listar(pageable);
	}

	@PostMapping
	@ResponseBody
	public ResponseEntity<Object> agregar(@Valid @RequestBody EstadoActivo estadoActivo, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return new ResponseEntity<Object>(
					new Mensaje(false, "Error " + bindingResult.getAllErrors().get(0).getDefaultMessage()),
					HttpStatus.BAD_REQUEST);
		} else {
			return estadoActivoService.agregar(estadoActivo).getCodigo() != 0
					? new ResponseEntity<Object>(new Mensaje(true, "Estado agregado con exito."), HttpStatus.OK)
					: new ResponseEntity<Object>(new Mensaje(false, "Error al agregar el estado."),
							HttpStatus.BAD_REQUEST);
		}
	}

	@PutMapping
	@ResponseBody
	public ResponseEntity<Object> modificar(@Valid @RequestBody EstadoActivo estadoActivo,
			BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return new ResponseEntity<Object>(
					new Mensaje(false, "Error " + bindingResult.getAllErrors().get(0).getDefaultMessage()),
					HttpStatus.BAD_REQUEST);
		} else {
			return estadoActivoService.modificar(estadoActivo).equals(estadoActivo)
					? new ResponseEntity<Object>(new Mensaje(true, "Estado modificado con exito."), HttpStatus.OK)
					: new ResponseEntity<Object>(new Mensaje(false, "Error al modificar el estado."),
							HttpStatus.BAD_REQUEST);
		}

	}

	@PutMapping(value = "{codigo}/activar")
	@ResponseBody
	public ResponseEntity<Object> activar(@PathVariable("codigo") long codigo) {
		Optional<EstadoActivo> object = estadoActivoService.buscarPorId(codigo);
		if (object.isPresent()) {
			return estadoActivoService.activar(object.get()).getEstado() == 1
					? new ResponseEntity<Object>(new Mensaje(true, "Estado activado con exito."), HttpStatus.OK)
					: new ResponseEntity<Object>(new Mensaje(false, "Error al activar el estado."),
							HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<Object>(new Mensaje(false, "El estado no existe."), HttpStatus.BAD_REQUEST);
		}

	}

	@PutMapping(value = "{codigo}/inactivar")
	@ResponseBody
	public ResponseEntity<Object> inactivar(@PathVariable("codigo") long codigo) {
		Optional<EstadoActivo> object = estadoActivoService.buscarPorId(codigo);
		if (object.isPresent()) {
			return estadoActivoService.inactivar(object.get()).getEstado() == 0
					? new ResponseEntity<Object>(new Mensaje(true, "Estado inactivado con exito."), HttpStatus.OK)
					: new ResponseEntity<Object>(new Mensaje(false, "Error al inactivar el estado."),
							HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<Object>(new Mensaje(false, "El estado no existe."), HttpStatus.BAD_REQUEST);
		}
	}

}
