package co.com.grupoasd.prueba_tenica.web.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import co.com.grupoasd.prueba_tenica.model.Mensaje;
import co.com.grupoasd.prueba_tenica.model.Persona;
import co.com.grupoasd.prueba_tenica.service.PersonaService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("api/persona")
public class PersonaActivoController {

	@Autowired
	PersonaService personaService;

	@GetMapping(value = "id/{codigo}")
	@ResponseBody
	public ResponseEntity<Object> buscarPorId(@PathVariable("codigo") long codigo) {
		Optional<Persona> object = personaService.buscarPorId(codigo);
		return object.isPresent() ? new ResponseEntity<Object>(object, HttpStatus.OK)
				: new ResponseEntity<Object>(new Mensaje(false, "Ningún registro encontrado."), HttpStatus.NOT_FOUND);
	}

	@GetMapping
	@ResponseBody
	public List<Persona> listar() {
		return personaService.listar();
	}

	@GetMapping(value = "paginacion")
	@ResponseBody
	public Page<Persona> listarPaginado(@PageableDefault(size = 5) @SortDefault.SortDefaults({
			@SortDefault(sort = "codigo", direction = Sort.Direction.ASC) }) Pageable pageable) {
		return personaService.listar(pageable);
	}

	@PostMapping
	@ResponseBody
	public ResponseEntity<Object> agregar(@Valid @RequestBody Persona persona, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return new ResponseEntity<Object>(
					new Mensaje(false, "Error " + bindingResult.getAllErrors().get(0).getDefaultMessage()),
					HttpStatus.BAD_REQUEST);
		} else {
			return personaService.agregar(persona).getCodigo() != 0
					? new ResponseEntity<Object>(new Mensaje(true, "Persona agregada con exito."), HttpStatus.OK)
					: new ResponseEntity<Object>(new Mensaje(false, "Error al agregar la persona."),
							HttpStatus.BAD_REQUEST);
		}
	}

	@PutMapping
	@ResponseBody
	public ResponseEntity<Object> modificar(@Valid @RequestBody Persona persona, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return new ResponseEntity<Object>(
					new Mensaje(false, "Error " + bindingResult.getAllErrors().get(0).getDefaultMessage()),
					HttpStatus.BAD_REQUEST);
		} else {
			return personaService.modificar(persona).equals(persona)
					? new ResponseEntity<Object>(new Mensaje(true, "Persona modificada con exito."), HttpStatus.OK)
					: new ResponseEntity<Object>(new Mensaje(false, "Error al modificar la persona."),
							HttpStatus.BAD_REQUEST);
		}

	}

}
