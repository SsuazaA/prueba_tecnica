package co.com.grupoasd.prueba_tenica.web.controller;

import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import co.com.grupoasd.prueba_tenica.model.Mensaje;

@ControllerAdvice
public class ExceptionHandlerAdvice {

	@ExceptionHandler(HttpMessageNotReadableException.class)
	public ResponseEntity<Object> handleException(HttpMessageNotReadableException e) {
		return new ResponseEntity<Object>(new Mensaje(false, "Se necesitan parámetros para ejecutar esta acción."),
				HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(MethodArgumentTypeMismatchException.class)
	public ResponseEntity<Object> handleException(MethodArgumentTypeMismatchException e) {
		return new ResponseEntity<Object>(new Mensaje(false, "Parámetros de entrada incorrectos."),
				HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(InvalidDataAccessApiUsageException.class)
	public ResponseEntity<Object> handleException(InvalidDataAccessApiUsageException e) {
		return new ResponseEntity<Object>(new Mensaje(false, "Se necesitan parámetros para ejecutar esta acción."),
				HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	

}
