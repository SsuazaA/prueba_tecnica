package co.com.grupoasd.prueba_tenica.web.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import co.com.grupoasd.prueba_tenica.model.Ciudad;
import co.com.grupoasd.prueba_tenica.model.Mensaje;
import co.com.grupoasd.prueba_tenica.service.CiudadService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("api/ciudad")
public class CiudadController {

	@Autowired
	CiudadService ciudadService;

	@GetMapping(value = "id/{codigo}")
	@ResponseBody
	public ResponseEntity<Object> buscarPorId(@PathVariable("codigo") long codigo) {
		Optional<Ciudad> object = ciudadService.buscarPorId(codigo);
		return object.isPresent() ? new ResponseEntity<Object>(object, HttpStatus.OK)
				: new ResponseEntity<Object>(new Mensaje(false, "Ningún registro encontrado."), HttpStatus.NOT_FOUND);
	}

	@GetMapping
	@ResponseBody
	public List<Ciudad> listar() {
		return ciudadService.listar();
	}

	@GetMapping(value = "paginacion")
	@ResponseBody
	public Page<Ciudad> listarPaginado(@PageableDefault(size = 5) @SortDefault.SortDefaults({
			@SortDefault(sort = "codigo", direction = Sort.Direction.ASC) }) Pageable pageable) {
		return ciudadService.listar(pageable);
	}

}
