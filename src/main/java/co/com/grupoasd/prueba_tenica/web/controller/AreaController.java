package co.com.grupoasd.prueba_tenica.web.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import co.com.grupoasd.prueba_tenica.model.Area;
import co.com.grupoasd.prueba_tenica.model.Mensaje;
import co.com.grupoasd.prueba_tenica.service.AreaService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("api/area")
public class AreaController {

	@Autowired
	AreaService areaService;

	@GetMapping(value = "id/{codigo}")
	@ResponseBody
	public ResponseEntity<Object> buscarPorId(@PathVariable("codigo") long codigo) {
		Optional<Area> object = areaService.buscarPorId(codigo);
		return object.isPresent() ? new ResponseEntity<Object>(object, HttpStatus.OK)
				: new ResponseEntity<Object>(new Mensaje(false, "Ningún registro encontrado."), HttpStatus.NOT_FOUND);
	}

	@GetMapping
	@ResponseBody
	public List<Area> listar() {
		return areaService.listar();
	}

	@GetMapping(value = "paginacion")
	@ResponseBody
	public Page<Area> listarPaginado(@PageableDefault(size = 5) @SortDefault.SortDefaults({
			@SortDefault(sort = "codigo", direction = Sort.Direction.ASC) }) Pageable pageable) {
		return areaService.listar(pageable);
	}

	@PostMapping
	@ResponseBody
	public ResponseEntity<Object> agregar(@Valid @RequestBody Area area, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return new ResponseEntity<Object>(
					new Mensaje(false, "Error " + bindingResult.getAllErrors().get(0).getDefaultMessage()),
					HttpStatus.BAD_REQUEST);
		} else {
			return areaService.agregar(area).getCodigo() != 0
					? new ResponseEntity<Object>(new Mensaje(true, "Area agregada con exito."), HttpStatus.OK)
					: new ResponseEntity<Object>(new Mensaje(false, "Error al agregar el area."),
							HttpStatus.BAD_REQUEST);
		}
	}

	@PutMapping
	@ResponseBody
	public ResponseEntity<Object> modificar(@Valid @RequestBody Area area, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return new ResponseEntity<Object>(
					new Mensaje(false, "Error " + bindingResult.getAllErrors().get(0).getDefaultMessage()),
					HttpStatus.BAD_REQUEST);
		} else {
			return areaService.modificar(area).equals(area)
					? new ResponseEntity<Object>(new Mensaje(true, "Area modificada con exito."), HttpStatus.OK)
					: new ResponseEntity<Object>(new Mensaje(false, "Error al modificar el area."),
							HttpStatus.BAD_REQUEST);
		}

	}

}
