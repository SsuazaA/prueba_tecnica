package co.com.grupoasd.prueba_tenica.web.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import co.com.grupoasd.prueba_tenica.model.Activo;
import co.com.grupoasd.prueba_tenica.model.Mensaje;
import co.com.grupoasd.prueba_tenica.service.ActivoService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("api/activo")
public class ActivoController {

	@Autowired
	ActivoService activoService;

	@GetMapping(value = "id/{codigo}")
	@ResponseBody
	public ResponseEntity<Object> buscarPorId(@PathVariable("codigo") long codigo) {
		Optional<Activo> object = activoService.buscarPorId(codigo);
		return object.isPresent() ? new ResponseEntity<Object>(object, HttpStatus.OK)
				: new ResponseEntity<Object>(new Mensaje(false, "Ningún registro encontrado."), HttpStatus.NOT_FOUND);
	}

	@GetMapping
	@ResponseBody
	public List<Activo> listar() {
		return activoService.listar();
	}

	@GetMapping(value = "paginacion")
	@ResponseBody
	public Page<Activo> listarPaginado(@PageableDefault(size = 5) @SortDefault.SortDefaults({
			@SortDefault(sort = "codigo", direction = Sort.Direction.ASC) }) Pageable pageable) {
		return activoService.listar(pageable);
	}

	@GetMapping(value = "estado/{codigo}")
	@ResponseBody
	public Page<Activo> listarEstadoPaginado(@PathVariable("codigo") long codigo,
			@PageableDefault(size = 5) @SortDefault.SortDefaults({
					@SortDefault(sort = "codigo", direction = Sort.Direction.ASC) }) Pageable pageable) {
		return activoService.listarPorEstado(codigo, pageable);
	}

	@PostMapping
	@ResponseBody
	public ResponseEntity<Object> agregar(@Valid @RequestBody Activo activo, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return new ResponseEntity<Object>(
					new Mensaje(false, "Error " + bindingResult.getAllErrors().get(0).getDefaultMessage()),
					HttpStatus.BAD_REQUEST);
		} else {
			return activoService.agregar(activo).getCodigo() != 0
					? new ResponseEntity<Object>(new Mensaje(true, "Activo agregado con exito."), HttpStatus.OK)
					: new ResponseEntity<Object>(new Mensaje(false, "Error al agregar el activo."),
							HttpStatus.BAD_REQUEST);
		}
	}

	@PutMapping
	@ResponseBody
	public ResponseEntity<Object> modificar(@Valid @RequestBody Activo activo, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return new ResponseEntity<Object>(
					new Mensaje(false, "Error " + bindingResult.getAllErrors().get(0).getDefaultMessage()),
					HttpStatus.BAD_REQUEST);
		} else {
			activoService.modificar(activo);
			return new ResponseEntity<Object>(new Mensaje(true, "Activo modificado con exito."), HttpStatus.OK);
		}

	}
}
