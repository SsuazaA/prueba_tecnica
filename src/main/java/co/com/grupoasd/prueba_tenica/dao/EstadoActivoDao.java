package co.com.grupoasd.prueba_tenica.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.com.grupoasd.prueba_tenica.model.EstadoActivo;

@Repository
public interface EstadoActivoDao extends JpaRepository<EstadoActivo, Long> {

	public List<EstadoActivo> findAllByEstadoOrderByCodigoAsc(int estado);

}
