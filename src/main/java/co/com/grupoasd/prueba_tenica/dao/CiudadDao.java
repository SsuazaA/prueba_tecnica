package co.com.grupoasd.prueba_tenica.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.com.grupoasd.prueba_tenica.model.Ciudad;

@Repository
public interface CiudadDao extends JpaRepository<Ciudad, Long> {

	public List<Ciudad> findAllByOrderByNombreAsc();
	
}
