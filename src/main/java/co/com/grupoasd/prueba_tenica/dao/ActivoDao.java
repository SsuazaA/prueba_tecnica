package co.com.grupoasd.prueba_tenica.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.com.grupoasd.prueba_tenica.model.Activo;

@Repository
public interface ActivoDao extends JpaRepository<Activo, Long> {

	public List<Activo> findAllByEstadoCodigo(long codigo);
	
}
