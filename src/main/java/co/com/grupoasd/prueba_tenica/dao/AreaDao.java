package co.com.grupoasd.prueba_tenica.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.com.grupoasd.prueba_tenica.model.Area;

@Repository
public interface AreaDao extends JpaRepository<Area, Long> {

	public List<Area> findAllByOrderByNombreAsc();

}
