package co.com.grupoasd.prueba_tenica.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.com.grupoasd.prueba_tenica.model.ActivoAsignado;

@Repository
public interface ActivoAsignadoDao extends JpaRepository<ActivoAsignado, Long> {
	
}
