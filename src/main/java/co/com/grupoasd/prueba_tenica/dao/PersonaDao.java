package co.com.grupoasd.prueba_tenica.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.com.grupoasd.prueba_tenica.model.Persona;

@Repository
public interface PersonaDao extends JpaRepository<Persona, Long> {

	public List<Persona> findAllByOrderByCodigoAsc();

}
