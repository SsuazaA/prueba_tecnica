package co.com.grupoasd.prueba_tenica.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.com.grupoasd.prueba_tenica.model.TipoActivo;

@Repository
public interface TipoActivoDao extends JpaRepository<TipoActivo, Long> {

	public List<TipoActivo> findAllByEstadoOrderByNombreAsc(int estado);

}
