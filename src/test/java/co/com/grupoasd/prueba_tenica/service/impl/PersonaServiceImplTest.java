package co.com.grupoasd.prueba_tenica.service.impl;

import static org.junit.Assert.assertTrue;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;

import co.com.grupoasd.prueba_tenica.model.Persona;
import co.com.grupoasd.prueba_tenica.service.PersonaService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PersonaServiceImplTest {

	@Autowired
	private PersonaService personaService;

	@Test
	@Rollback
	public void testAgregar() {
		Persona persona = new Persona(new Long(1));
		persona.setIdentificacion("1052165");
		persona.setPrimerNombre("Primer Nombre");
		persona.setSegundoNombre("Segundo Nombre");
		persona.setPrimerApellido("Primer Apellido");
		persona.setSegundoApellido("Segundo Apellido");
		personaService.agregar(persona);
		assertTrue(personaService.buscarPorId(new Long(1)).isPresent());
	}

	@Test
	@Rollback
	public void testModificar() {
		Persona persona = new Persona(new Long(1));
		persona.setIdentificacion("1052165");
		persona.setPrimerNombre("Primer Nombre");
		persona.setSegundoNombre("Segundo Nombre");
		persona.setPrimerApellido("Primer Apellido");
		persona.setSegundoApellido("Segundo Apellido");
		personaService.agregar(persona);

		Optional<Persona> persona_ = personaService.buscarPorId(new Long(1));
		persona_.get().setPrimerNombre("Primer nombre editado");
		personaService.modificar(persona_.get());
		assertTrue(personaService.buscarPorId(new Long(1)).get().getPrimerNombre().equals("Primer nombre editado"));
	}

	@Test
	@Rollback
	public void testBuscarPorId() {
		Persona persona = new Persona(new Long(1));
		persona.setIdentificacion("1052165");
		persona.setPrimerNombre("Primer Nombre");
		persona.setSegundoNombre("Segundo Nombre");
		persona.setPrimerApellido("Primer Apellido");
		persona.setSegundoApellido("Segundo Apellido");
		personaService.agregar(persona);

		assertTrue(personaService.buscarPorId(new Long(1)).isPresent());
		assertTrue(!personaService.buscarPorId(new Long(2000)).isPresent());
	}

	@Test
	@Rollback
	public void testListar() {
		Persona persona = new Persona(new Long(1));
		persona.setIdentificacion("1052165");
		persona.setPrimerNombre("Primer Nombre");
		persona.setSegundoNombre("Segundo Nombre");
		persona.setPrimerApellido("Primer Apellido");
		persona.setSegundoApellido("Segundo Apellido");
		personaService.agregar(persona);
		assertTrue(personaService.listar().size() > 0);
	}

}
