package co.com.grupoasd.prueba_tenica.service.impl;

import static org.junit.Assert.assertTrue;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;

import co.com.grupoasd.prueba_tenica.model.Area;
import co.com.grupoasd.prueba_tenica.model.Ciudad;
import co.com.grupoasd.prueba_tenica.service.AreaService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AreaServiceImplTest {

	@Autowired
	private AreaService areaService;

	@Test
	@Rollback
	public void testAgregar() {
		Area area = new Area(new Long(1));
		area.setNombre("Departamento de cartera");
		area.setDireccion("Direccion 1");
		area.setCiudad(new Ciudad(new Long(1)));
		areaService.agregar(area);
		assertTrue(areaService.buscarPorId(new Long(1)).isPresent());
	}

	@Test
	@Rollback
	public void testModificar() {
		Area area = new Area(new Long(1));
		area.setNombre("Departamento de cartera");
		area.setDireccion("Direccion 1");
		area.setCiudad(new Ciudad(new Long(1)));
		areaService.agregar(area);

		Optional<Area> area_ = areaService.buscarPorId(new Long(1));
		area_.get().setNombre("Area");
		areaService.modificar(area_.get());
		assertTrue(areaService.buscarPorId(new Long(1)).get().getNombre().equals("Area"));
	}

	@Test
	@Rollback
	public void testBuscarPorId() {
		Area area = new Area(new Long(1));
		area.setNombre("Departamento de cartera");
		area.setDireccion("Direccion 1");
		area.setCiudad(new Ciudad(new Long(1)));
		areaService.agregar(area);

		assertTrue(areaService.buscarPorId(new Long(1)).isPresent());
		assertTrue(!areaService.buscarPorId(new Long(2000)).isPresent());
	}

	@Test
	@Rollback
	public void testListar() {
		Area area = new Area(new Long(1));
		area.setNombre("Departamento de cartera");
		area.setDireccion("Direccion 1");
		area.setCiudad(new Ciudad(new Long(1)));
		areaService.agregar(area);
		assertTrue(areaService.listar().size() > 0);
	}

}
