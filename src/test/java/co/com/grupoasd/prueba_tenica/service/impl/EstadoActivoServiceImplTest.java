package co.com.grupoasd.prueba_tenica.service.impl;

import static org.junit.Assert.assertTrue;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;

import co.com.grupoasd.prueba_tenica.model.EstadoActivo;
import co.com.grupoasd.prueba_tenica.service.EstadoActivoService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EstadoActivoServiceImplTest {

	@Autowired
	private EstadoActivoService estadoActivoService;

	@Test
	@Rollback
	public void testAgregar() {
		EstadoActivo estadoActivo = new EstadoActivo(new Long(1));
		estadoActivo.setNombre("nombre 1");
		estadoActivoService.agregar(estadoActivo);
		assertTrue(estadoActivoService.buscarPorId(new Long(1)).isPresent());
	}

	@Test
	@Rollback
	public void testModificar() {
		EstadoActivo estadoActivo = new EstadoActivo(new Long(1));
		estadoActivo.setNombre("nombre 1");
		estadoActivoService.agregar(estadoActivo);

		Optional<EstadoActivo> estadoActivo_ = estadoActivoService.buscarPorId(new Long(1));
		estadoActivo_.get().setNombre("Area");
		estadoActivoService.modificar(estadoActivo_.get());
		assertTrue(estadoActivoService.buscarPorId(new Long(1)).get().getNombre().equals("Area"));
	}

	@Test
	@Rollback
	public void testActivar() {
		EstadoActivo estadoActivo = new EstadoActivo(new Long(1));
		estadoActivo.setNombre("nombre 1");
		estadoActivo.setEstado(0);
		estadoActivoService.agregar(estadoActivo);

		Optional<EstadoActivo> estadoActivo_ = estadoActivoService.buscarPorId(new Long(1));
		estadoActivoService.activar(estadoActivo_.get());
		assertTrue(estadoActivoService.buscarPorId(new Long(1)).get().getEstado() == 1);
	}

	@Test
	@Rollback
	public void testInactivar() {
		EstadoActivo estadoActivo = new EstadoActivo(new Long(1));
		estadoActivo.setNombre("nombre 1");
		estadoActivo.setEstado(1);
		estadoActivoService.agregar(estadoActivo);

		Optional<EstadoActivo> estadoActivo_ = estadoActivoService.buscarPorId(new Long(1));
		estadoActivoService.inactivar(estadoActivo_.get());
		assertTrue(estadoActivoService.buscarPorId(new Long(1)).get().getEstado() == 0);
	}

	@Test
	@Rollback
	public void testBuscarPorId() {
		EstadoActivo estadoActivo = new EstadoActivo(new Long(1));
		estadoActivo.setNombre("nombre 1");
		estadoActivoService.agregar(estadoActivo);

		assertTrue(estadoActivoService.buscarPorId(new Long(1)).isPresent());
		assertTrue(!estadoActivoService.buscarPorId(new Long(2000)).isPresent());
	}

	@Test
	@Rollback
	public void testListar() {
		EstadoActivo estadoActivo = new EstadoActivo(new Long(1));
		estadoActivo.setNombre("nombre 1");
		estadoActivoService.agregar(estadoActivo);
		assertTrue(estadoActivoService.listar().size() > 0);
	}

}
