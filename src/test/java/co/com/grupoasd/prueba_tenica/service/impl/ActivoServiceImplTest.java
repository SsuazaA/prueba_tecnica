package co.com.grupoasd.prueba_tenica.service.impl;

import static org.junit.Assert.assertTrue;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;

import co.com.grupoasd.prueba_tenica.model.Activo;
import co.com.grupoasd.prueba_tenica.model.EstadoActivo;
import co.com.grupoasd.prueba_tenica.model.TipoActivo;
import co.com.grupoasd.prueba_tenica.service.ActivoService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ActivoServiceImplTest {

	@Autowired
	private ActivoService activoService;

	@Test
	@Rollback
	public void testAgregar() {
		Activo activo = new Activo();
		activo.setCodigo(new Long(1));
		activo.setTipoActivo(new TipoActivo(new Long(1)));
		activo.setEstado(new EstadoActivo(new Long(1)));
		activoService.agregar(activo);
		assertTrue(activoService.buscarPorId(new Long(1)).isPresent());
	}

	@Test
	@Rollback
	public void testModificar() {
		Activo activo = new Activo();
		activo.setCodigo(new Long(1));
		activo.setTipoActivo(new TipoActivo(new Long(1)));
		activo.setEstado(new EstadoActivo(new Long(1)));
		activo.setColor("Cafe");
		activoService.agregar(activo);

		Optional<Activo> activo_ = activoService.buscarPorId(new Long(1));
		activo_.get().setColor("Amarillo");
		activoService.modificar(activo_.get());

		assertTrue(activoService.buscarPorId(new Long(1)).get().getColor().equals("Amarillo"));
	}

	@Test
	@Rollback
	public void testBuscarPorId() {
		Activo activo = new Activo();
		activo.setCodigo(new Long(1));
		activo.setTipoActivo(new TipoActivo(new Long(1)));
		activo.setEstado(new EstadoActivo(new Long(1)));
		activoService.agregar(activo);

		assertTrue(activoService.buscarPorId(new Long(1)).isPresent());
		assertTrue(!activoService.buscarPorId(new Long(2000)).isPresent());
	}

	@Test
	@Rollback
	public void testListar() {
		Activo activo = new Activo();
		activo.setCodigo(new Long(1));
		activo.setTipoActivo(new TipoActivo(new Long(1)));
		activo.setEstado(new EstadoActivo(new Long(1)));
		activoService.agregar(activo);

		assertTrue(activoService.listar().size() > 0);
	}

}
