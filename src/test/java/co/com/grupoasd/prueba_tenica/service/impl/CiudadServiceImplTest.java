package co.com.grupoasd.prueba_tenica.service.impl;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;

import co.com.grupoasd.prueba_tenica.dao.CiudadDao;
import co.com.grupoasd.prueba_tenica.model.Ciudad;
import co.com.grupoasd.prueba_tenica.service.CiudadService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CiudadServiceImplTest {

	@Autowired
	private CiudadDao ciudadDao;

	@Autowired
	private CiudadService ciudadService;

	@Test
	@Rollback
	public void testBuscarPorId() {
		Ciudad ciudad = new Ciudad(new Long(1));
		ciudadDao.save(ciudad);

		assertTrue(ciudadService.buscarPorId(new Long(1)).isPresent());
		assertTrue(!ciudadService.buscarPorId(new Long(2000)).isPresent());
	}

	@Test
	@Rollback
	public void testListar() {
		Ciudad ciudad = new Ciudad(new Long(1));
		ciudadDao.save(ciudad);
		assertTrue(ciudadService.listar().size() > 0);
	}

}
