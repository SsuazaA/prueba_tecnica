package co.com.grupoasd.prueba_tenica.service.impl;

import static org.junit.Assert.assertTrue;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;

import co.com.grupoasd.prueba_tenica.model.TipoActivo;
import co.com.grupoasd.prueba_tenica.service.TipoActivoService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TipoActivoServiceImplTest {

	@Autowired
	private TipoActivoService tipoActivoService;

	@Test
	@Rollback
	public void testAgregar() {
		TipoActivo tipoActivo = new TipoActivo(new Long(1));
		tipoActivo.setNombre("nombre 1");
		tipoActivoService.agregar(tipoActivo);
		assertTrue(tipoActivoService.buscarPorId(new Long(1)).isPresent());
	}

	@Test
	@Rollback
	public void testModificar() {
		TipoActivo tipoActivo = new TipoActivo(new Long(1));
		tipoActivo.setNombre("nombre 1");
		tipoActivoService.agregar(tipoActivo);

		Optional<TipoActivo> tipoActivo_ = tipoActivoService.buscarPorId(new Long(1));
		tipoActivo_.get().setNombre("nombre editado 1");
		tipoActivoService.modificar(tipoActivo_.get());
		assertTrue(tipoActivoService.buscarPorId(new Long(1)).get().getNombre().equals("nombre editado 1"));
	}

	@Test
	@Rollback
	public void testActivar() {
		TipoActivo tipoActivo = new TipoActivo(new Long(1));
		tipoActivo.setNombre("nombre 1");
		tipoActivo.setEstado(0);
		tipoActivoService.agregar(tipoActivo);

		Optional<TipoActivo> tipoActivo_ = tipoActivoService.buscarPorId(new Long(1));
		tipoActivoService.activar(tipoActivo_.get());
		assertTrue(tipoActivoService.buscarPorId(new Long(1)).get().getEstado() == 1);
	}

	@Test
	@Rollback
	public void testInactivar() {
		TipoActivo tipoActivo = new TipoActivo(new Long(1));
		tipoActivo.setNombre("nombre 1");
		tipoActivo.setEstado(1);
		tipoActivoService.agregar(tipoActivo);

		Optional<TipoActivo> tipoActivo_ = tipoActivoService.buscarPorId(new Long(1));
		tipoActivoService.inactivar(tipoActivo_.get());
		assertTrue(tipoActivoService.buscarPorId(new Long(1)).get().getEstado() == 0);
	}

	@Test
	@Rollback
	public void testBuscarPorId() {
		TipoActivo tipoActivo = new TipoActivo(new Long(1));
		tipoActivo.setNombre("nombre 1");
		tipoActivoService.agregar(tipoActivo);

		assertTrue(tipoActivoService.buscarPorId(new Long(1)).isPresent());
		assertTrue(!tipoActivoService.buscarPorId(new Long(2000)).isPresent());
	}

	@Test
	@Rollback
	public void testListar() {
		TipoActivo tipoActivo = new TipoActivo(new Long(1));
		tipoActivo.setNombre("nombre 1");
		tipoActivoService.agregar(tipoActivo);
		assertTrue(tipoActivoService.listar().size() > 0);
	}

}
